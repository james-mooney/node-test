module.exports = (backingStore) => {
  const count = (userId) => {
    const streams = backingStore[userId]
    return streams ? streams.size : 0
  }

  const tryAdd = (userId, streamId) => {
    // There is a race condition where two threads will see no user
    // exists, the first adds an empty set and then populates it
    // and the 2nd thread proceeds to add an empty set
    // In reality this should be something like a redis instance and this should
    // be made into an atomic operation.
    if (!backingStore[userId]) {
      backingStore[userId] = new Set()
    };

    if (count(userId) < 3) {
      backingStore[userId].add(streamId)
      return true
    }

    return false
  }

  return {
    count,
    tryAdd
  }
}
