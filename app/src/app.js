const express = require('express')
const bodyParser = require('body-parser')

module.exports = (database) => {
  const app = express()
  app.use(bodyParser.json())

  app.get('/', (req, res) => {
    return res.status(200).send('Alive')
  })

  app.get('/:userId/count', (req, res) => {
    const count = database.count(req.params.userId)
    return res.status(200).json({ count: count })
  })

  app.post('/event', (req, res) => {
    const eventType = req.body.eventType
    const userId = req.body.userId
    const streamId = req.body.streamId

    if (!eventType || !userId || !streamId) {
      return res.status(400).send('invalid body')
    }

    if (eventType === 'add') {
      const added = database.tryAdd(userId, streamId)
      if (added) {
        return res.status(200).send('OK')
      }

      return res.status(403).send('too many active streams')
    }

    return res.status(500).send('invalid event type')
  })

  return app
}
