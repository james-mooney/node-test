const App = require('./src/app')
const Db = require('./src/db')

const database = Db({})

const port = 8001

const app = App(database)

app.listen(port)
