const App = require('../src/app')
const Db = require('../src/db')
const request = require('supertest')

describe('api', () => {
  let app
  let backingStore
  beforeEach(() => {
    backingStore = {
      'test-user': new Set(['Stream1', 'Stream2', 'Stream3'])
    }
    const db = Db(backingStore)
    app = App(db)
  })

  describe('root', () => {
    it('returns alive', async () => {
      const response = await request(app).get('/')

      expect(response.status).toEqual(200)
      expect(response.text).toEqual('Alive')
    })
  })

  describe('count endpoint', () => {
    it('returns number of streams user is currently subscribed to', async () => {
      const response = await request(app).get('/test-user/count')

      expect(response.status).toEqual(200)
      expect(response.body).toEqual({ count: 3 })
    })
  })

  describe('event endpoint', () => {
    describe('with bad request', () => {
      it('returns 500 with no event type', async () => {
        const body = {
          userId: 'test-user',
          streamId: 'stream1'
        }

        const response = await request(app)
          .post('/event')
          .send(body)
          .set('Content-Type', 'application/json')

        expect(response.status).toEqual(400)
        expect(response.text).toEqual('invalid body')
      })

      it('returns 500 with no user id', async () => {
        const body = {
          eventType: 'add',
          streamId: 'stream1'
        }

        const response = await request(app)
          .post('/event')
          .send(body)
          .set('Content-Type', 'application/json')

        expect(response.status).toEqual(400)
        expect(response.text).toEqual('invalid body')
      })

      it('returns 500 with no stream id', async () => {
        const body = {
          eventType: 'add',
          userId: 'test-user'
        }

        const response = await request(app)
          .post('/event')
          .send(body)
          .set('Content-Type', 'application/json')

        expect(response.status).toEqual(400)
        expect(response.text).toEqual('invalid body')
      })
    })

    describe('add event', () => {
      it('adds a stream id to the user with less than 3 streams active', async () => {
        const body = {
          eventType: 'add',
          userId: 'new-user',
          streamId: 'stream1'
        }

        const response = await request(app)
          .post('/event')
          .send(body)
          .set('Content-Type', 'application/json')

        expect(response.status).toEqual(200)
        expect(response.text).toEqual('OK')
        expect(backingStore['new-user']).toEqual(new Set(['stream1']))
      })

      it('does not add a stream id to a user with 3 streams', async () => {
        const body = {
          eventType: 'add',
          userId: 'test-user',
          streamId: 'Stream4'
        }

        const response = await request(app)
          .post('/event')
          .send(body)
          .set('Content-Type', 'application/json')

        expect(response.status).toEqual(403)
        expect(response.text).toEqual('too many active streams')
        expect(backingStore['test-user']).toEqual(new Set(['Stream1', 'Stream2', 'Stream3']))
      })
    })
  })
})
