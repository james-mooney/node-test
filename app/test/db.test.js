const Db = require('../src/db')

describe('count', () => {
  it('returns count of a users active streams', () => {
    const backingStore = { 'test-user': new Set(['stream1', 'stream2']) }
    const db = Db(backingStore)

    const result = db.count('test-user')

    expect(result).toEqual(2)
  })

  it('returns 0 if user is not found', () => {
    const db = Db({})

    const result = db.count('test-user')

    expect(result).toEqual(0)
  })
})

describe('tryAdd', () => {
  it('adds a stream to the users active streams', () => {
    const backingStore = { 'test-user': new Set() }
    const db = Db(backingStore)

    const result = db.tryAdd('test-user', 'stream')

    expect(result).toEqual(true)
    expect(backingStore).toEqual({ 'test-user': new Set(['stream']) })
  })

  it('adds a new user with the stream if one is not found', () => {
    const backingStore = {}
    const db = Db(backingStore)

    const result = db.tryAdd('test-user', 'stream')

    expect(result).toEqual(true)
    expect(backingStore).toEqual({ 'test-user': new Set(['stream']) })
  })

  it('only allows a stream to be added once', () => {
    // The assumption here is that every stream has a unique id
    // i.e. the unique id will store information somewhere about what stream it actually is
    // as a result duplicate messages get somewhat handled. In reality you would likely have
    // an event id which is unique to the message.
    const backingStore = { 'test-user': new Set(['stream']) }
    const db = Db(backingStore)

    const result1 = db.tryAdd('test-user', 'stream')
    const result2 = db.tryAdd('test-user', 'stream')

    expect(result1).toEqual(true)
    expect(result2).toEqual(true)
    expect(backingStore).toEqual({
      'test-user': new Set(['stream'])
    })
  })

  it('does not allow an stream to be added when there are already 3', () => {
    const backingStore = { 'test-user': new Set(['Stream1', 'Stream2', 'Stream3']) }
    const db = Db(backingStore)

    const result = db.tryAdd('test-user', 'Stream4')

    expect(result).toEqual(false)
    expect(backingStore).toEqual(backingStore)
  })
})
