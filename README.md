# RUNNING INSTRUCTIONS
`cd app`
`npm install`
`npm start` 

Two endpoints are exposed on port 8001

GET /{userId}/count
> Gets a count of streams the user is subscribed to

POST /event
Content-Type: application/json
{
    eventType: "add",
    userId: "test-user",
    streamId: "Stream1"
}
> Subscribes a user to a new stream

`curl -X POST localhost:8001/event --data '{"eventType":"add",serId":"test-user","streamId":"stream4"}' -H "Content-Type: application/json"`

`curl localhost:8001/test-user/count`


# SCALING
Currently this does not scale at all. There is an unhandled race condition in the tryAdd function in app/src/db.js so starting two streams for the same user at once could cause immediate problems.

To allow this to scale the backingStore should be replaced by something like a redis instance. By making all the transactions atomic it is possible to run more than a single instance of this code and point it to the same redis instance. The API can then be changed to lambda functions or dockerised and orchestrated. 

# ASSUMPTIONS
There are a few massive assumptions made: firstly, this API doesn't need to actually stream any videos and secondly, the stream id is produced by another/other system/s which control/s the streams which creates a unique ID for every single stream requested. This means any duplicate IDs are due to more than one message being posted rather than the same stream on multiple devices. 
